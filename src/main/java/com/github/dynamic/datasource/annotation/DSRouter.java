package com.github.dynamic.datasource.annotation;

import com.github.dynamic.datasource.constant.DataSourceNameConstant;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface DSRouter {
    String value() default DataSourceNameConstant.MASTER;
}
