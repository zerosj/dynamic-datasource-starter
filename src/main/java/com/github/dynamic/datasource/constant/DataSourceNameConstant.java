package com.github.dynamic.datasource.constant;

public final class DataSourceNameConstant {
    public static final String MASTER = "master";
    public static final String SLAVE= "slave";

}
